sf probe 0:0
env default -a
mtd list
load mmc 0 $load_addr 'spi_fw/bl2_qspi.pbl'
mtd erase bl2
mtd write bl2 $load_addr +$filesize
load mmc 0 $load_addr 'spi_fw/fip.bin'
mtd erase bl3
mtd write bl3 $load_addr +$filesize
load mmc 0 $load_addr 'mcfirmware/mc_ls1088a.itb'
mtd erase mcfirmware
mtd write mcfirmware $load_addr +$filesize
load mmc 0 $load_addr 'dpaa2config/eth-dpl-all.dtb'
mtd erase dpl
mtd write dpl $load_addr +$filesize
load mmc 0 $load_addr 'dpaa2config/dpc.0x1D-0x0D.dtb'
mtd erase dpc
mtd write dpc $load_addr +$filesize
load mmc 0 $load_addr 'spi_fw/fsl-ls1088a-ten64.dtb'
mtd erase devicetree
mtd write devicetree $load_addr +$filesize
