#!/bin/sh
set -e
ARCHIVE_MACHINE_PORT=${ARCHIVE_MACHINE_PORT:-22}

rsync_image() {
  rsync -e "ssh -p ${ARCHIVE_MACHINE_PORT} -i /tmp/archive_machine_user_key -o 'CheckHostIP no' -o 'UserKnownHostsFile /tmp/archive_machine_host_key'" $@
}
image_ssh() {
  ssh -p ${ARCHIVE_MACHINE_PORT} -i /tmp/archive_machine_user_key -o 'CheckHostIP no' -o 'UserKnownHostsFile /tmp/archive_machine_host_key' "${ARCHIVE_MACHINE_USER}@${ARCHIVE_MACHINE_HOST}" $@
}


SOURCE_DIR="${1}"
ARCHIVE_DEST_DIR="${2}"

echo "${ARCHIVE_DEST_DIR}"

echo "${ARCHIVE_MACHINE_USER_KEY}" >> /tmp/archive_machine_user_key

if [ "${ARCHIVE_MACHINE_PORT}" -eq "22" ]; then
	echo "${ARCHIVE_MACHINE_HOST} ${ARCHIVE_MACHINE_HOST_KEY}" >> /tmp/archive_machine_host_key
else
	echo "[${ARCHIVE_MACHINE_HOST}]:${ARCHIVE_MACHINE_PORT} ${ARCHIVE_MACHINE_HOST_KEY}" >> /tmp/archive_machine_host_key
fi

chmod 0600 /tmp/archive_machine_user_key
chmod 0600 /tmp/archive_machine_host_key
image_ssh "mkdir -p ${ARCHIVE_DEST_DIR}"

rsync_image --recursive --progress "${SOURCE_DIR}" "${ARCHIVE_MACHINE_USER}@${ARCHIVE_MACHINE_HOST}:${ARCHIVE_DEST_DIR}"
