#!/bin/sh

cd dtb

git clone --depth=1 --branch=v5.11  \
  https://source.codeaurora.org/external/qoriq/qoriq-components/linux \
  linux-dtb-compile

cd linux-dtb-compile

patch -p1 -i ../0001-arm64-dts-ls1088a-add-internal-PCS-for-DPMAC1-node.patch
patch -p1 -i ../0002-arm64-dts-ls1088a-add-missing-PMU-node.patch
patch -p1 -i ../0003-arm64-dts-add-device-tree-for-Traverse-Ten64-board.patch
patch -p1 -i ../0004-arm64-dts-ten64-remove-redundant-interrupt-declarati.patch

make ARCH=arm64 defconfig
make ARCH=arm64 freescale/fsl-ls1088a-ten64.dtb

cp arch/arm64/boot/dts/freescale/fsl-ls1088a-ten64.dtb ..
