
#!/bin/sh
set -e

BUILD=${BUILD:-debug}
BOARD=${BOARD:-ten64}
BUILD_ALL_SERDES=${BUILD_ALL_SERDES:-1}
FREQUENCY=${FREQUENCY:-1600}

build_qspi_bl2() {
    serdes_config=$1
    rcw_qspi=$(realpath "rcw/${BOARD}/${serdes_config}/rcw_${FREQUENCY}_qspi.bin")
    rm -f "arm-trusted-firmware/build/${BOARD}/${BUILD}/bl2.bin" "arm-trusted-firmware/build/${BOARD}/${BUILD}/bl2_qspi.pbl"
    make -C arm-trusted-firmware bl2 pbl CROSS_COMPILE="${CROSS_COMPILE}" PLAT="${BOARD}" BOOT_MODE=qspi BL33=${UBOOT} DDR_DEBUG=yes DEBUG=1 RCW="${rcw_qspi}"
}

DEFAULT_SERDES_CONFIG=${DEFAULT_SERDES_CONFIG:-"default_2x10g"}
RCW_SD=$(realpath "rcw/${BOARD}/${DEFAULT_SERDES_CONFIG}/rcw_${FREQUENCY}_sd.bin")
RCW_QSPI=$(realpath "rcw/${BOARD}/${DEFAULT_SERDES_CONFIG}/rcw_${FREQUENCY}_qspi.bin")

UBOOT=$(realpath u-boot/u-boot.bin)

make -C arm-trusted-firmware realclean

make -C arm-trusted-firmware pbl CROSS_COMPILE="${CROSS_COMPILE}" PLAT="${BOARD}" BOOT_MODE=sd RCW="${RCW_SD}" DDR_DEBUG=yes DEBUG=1

make -C arm-trusted-firmware fip CROSS_COMPILE="${CROSS_COMPILE}" PLAT="${BOARD}" BOOT_MODE=sd BL33=${UBOOT} RCW="${RCW_SD}" DDR_DEBUG=yes DEBUG=1
# For alternative configurations like (XG0,XG1) = (10G,1G) or (1G,1G)
if [ "${BUILD_ALL_SERDES}" = "1" ]; then
    build_qspi_bl2 "xg1_sfp_1G"
    cp "arm-trusted-firmware/build/${BOARD}/${BUILD}/bl2_qspi.pbl" "arm-trusted-firmware/build/${BOARD}/${BUILD}/bl2_qspi_xg1_1g.pbl"
    build_qspi_bl2 "both_sfp_1G"
    cp "arm-trusted-firmware/build/${BOARD}/${BUILD}/bl2_qspi.pbl" "arm-trusted-firmware/build/${BOARD}/${BUILD}/bl2_qspi_both_sfp_1g.pbl"
fi

build_qspi_bl2 "${DEFAULT_SERDES_CONFIG}"
